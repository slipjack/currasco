
# EtI Curriculum - TEST

Steps to use the [`currasco` commandline interface (CLI)](https://gitlab.com/ttc/currasco) to pull the (public) [`currasco-eti`](https://gitlab.com/slipjack/currasco-eti) content, identified through the `npm` registry, and build it locally.

## Install dependencies

1. Make sure you have at least read access to the [`currasco`](https://gitlab.com/ttc/currasco) project 
2. Install debian dependencies with the following:
```
sudo apt install build-essential libssl-dev pandoc atom
```
3. Install useful *atom* packages manually or by running the following:
```
apm install markdown-preview-enhanced git-plus atom-mermaid intentions busy-signal linter linter-ui-default linter-remark atom-markdown-table-editor
```

## Install and configure NVM

Run the following: 

```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" 
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
```

## Use NVM to install and select the correct version of nodejs

Open a **new Terminal** and run the following:

```
nvm install v8.9.0
nvm use v8.9.0
```

## Install node dependencies for, clone and globally install currasco

Run the following: 

```
npm install -g docsmith@beta
npm install -g react-native-scripts
cd ~/src
git clone git@gitlab.com:ttc/currasco.git
cd currasco
npm install -g
```

## Create and enter a workspace directory

Run the following: 

```
cd ~/src
mkdir currascotest
cd currascotest
```

## Clear out old content (if necessary)

Run the following: 

```
rmdir @currasco
rm -rf ~/.content/*
```

## Initialize content-as-code and pull the currasco-eti content package

Run the following: 

```
currasco init
```

## Fix weird bug (if necessary)

Run the following: 

```
sudo sysctl -w fs.inotify.max_user_instances=1024
sudo sysctl -w fs.inotify.max_user_watches=12288
```

## Build and preview 

1. Run the following: 
```
currasco start
```

2. Open `127.0.0.1:8081` in your browser

## Edit content

1. Launch *atom* and open one of the following files: 
    - `~/src/currascotest/@currasco/preview/index.md`
    - `~/src/currascotest/@currasco/preview/test-folder/index.md`
2. Edit the file and save your changes
3. Note the linting feedback in *atom*
4. Watch the preview in your browser

